package com.acousticm.mvppattern;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Zephy on 3/2/2018.
 */

public class NavigationUtils {
    public static void startActivity(Context context, Intent intent) {
        context.startActivity(intent);
    }

    public static Intent createIntent(Context context, Class clazz) {
        Intent intent = new Intent(context, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }
}
