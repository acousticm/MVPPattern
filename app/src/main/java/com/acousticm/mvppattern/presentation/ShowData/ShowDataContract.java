package com.acousticm.mvppattern.presentation.ShowData;

import android.widget.TextView;

import com.acousticm.mvppattern.bases.BasePresenter;
import com.acousticm.mvppattern.bases.BaseView;
import com.acousticm.mvppattern.model.GameofthroneModel;

import java.util.List;

/**
 * Created by Zephy on 3/2/2018.
 */

public class ShowDataContract {
    interface View extends BaseView<Presenter> {
        void onGetDataSuccess(String message, List<GameofthroneModel> data);
        void onGetDataFailure(String message);
    }

    interface Presenter extends BasePresenter {
        void getDataFromApi();
        void setTextCharacter(TextView tvCharacter);
        void setTextQuote(TextView tvQuote);
    }
}
