package com.acousticm.mvppattern.presentation.ShowData;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.acousticm.mvppattern.R;
import com.acousticm.mvppattern.bases.BaseActivity;
import com.acousticm.mvppattern.databinding.ActivityShowdataBinding;
import com.acousticm.mvppattern.model.GameofthroneModel;

import java.util.List;

import static android.support.v4.util.Preconditions.checkNotNull;


/**
 * Created by Zephy on 3/2/2018.
 */

public class ShowDataActivity extends BaseActivity implements ShowDataContract.View, ShowDataPresenter.onLoadingSuccess,
        View.OnClickListener{
    ShowDataContract.Presenter mPresenter;
    ActivityShowdataBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_showdata);
        initInstances();
    }

    public void initInstances() {
        // Initialization
        binding.btnRandom.setOnClickListener(this);
    }

    @Override
    public void onGetDataSuccess(String message, List<GameofthroneModel> data) {
        showMsg(message);
    }

    @Override
    public void onGetDataFailure(String message) {
        showMsg(message);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setPresenter(@NonNull ShowDataContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter = new ShowDataPresenter(this, this, this);
        mPresenter.getDataFromApi();
    }

    @Override
    public void onLoadingSuccess(String character, String quote) {
        mPresenter.setTextCharacter(binding.tvCharacter);
        mPresenter.setTextQuote(binding.tvQuote);
        showMsg("Success");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRandom:
                mPresenter.getDataFromApi();
                break;
        }
    }
}
