package com.acousticm.mvppattern.presentation.Hello;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Zephy on 3/1/2018.
 */

public class MainPresenter implements MainContract.Presenter {
    @NonNull
    MainContract.View mView;
    String text;
    Context mContext;

    MainPresenter(@NonNull MainContract.View view, Context context) {
        this.mView = view;
        this.mContext = context;
        mView.setPresenter(this);
    }

    @Override
    public boolean validateTextFields(EditText[] fields) {
        for (EditText field : fields) {
            if (field.getText().toString().isEmpty()) {
                field.setError("Provide the require field!");
                return false;
            }
        }
        validated(fields);
        return true;
    }

    @Override
    public boolean clearTextFields(EditText[] fields) {
        text = fields[0].getText().toString();
        validateClearText();
        return true;
    }

    private void validateClearText() {
        mView.clearText("");
        mView.showMessageSuccess("Clear Data Success");
    }

    private void validated(EditText[] fields) {
        text = fields[0].getText().toString();
        showEditTextMsg(text);
    }

    private void showEditTextMsg(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void start() {

    }
}
