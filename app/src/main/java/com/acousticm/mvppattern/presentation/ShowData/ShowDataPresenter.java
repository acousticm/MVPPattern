package com.acousticm.mvppattern.presentation.ShowData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;
import android.widget.Toast;

import com.acousticm.mvppattern.Service.HttpManager;
import com.acousticm.mvppattern.model.GameofthroneModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zephy on 3/2/2018.
 */

public class ShowDataPresenter implements ShowDataContract.Presenter {
    @NonNull
    ShowDataContract.View mView;
    Context mContext;
    String textCharacter, textQuote;
    onLoadingSuccess mListener;

    public interface onLoadingSuccess {
        void onLoadingSuccess(String character, String quote);
    }

    ShowDataPresenter(@NonNull ShowDataContract.View view, Context context, onLoadingSuccess mListener) {
        this.mView = view;
        this.mContext = context;
        this.mListener = mListener;
        mView.setPresenter(this);
    }

    @Override
    public void getDataFromApi() {
        Call<GameofthroneModel> call = HttpManager.getInstance().getService().getData();
        call.enqueue(new Callback<GameofthroneModel>() {
            @Override
            public void onResponse(Call<GameofthroneModel> call, Response<GameofthroneModel> response) {
                GameofthroneModel result = response.body();
                if (response.isSuccessful()) {
                    if (result != null) {
                        textQuote = result.getQuote();
                        textCharacter = result.getCharacter();
                        mListener.onLoadingSuccess(textCharacter, textQuote);
                    }
                }
            }

            @Override
            public void onFailure(Call<GameofthroneModel> call, Throwable t) {
                //
            }
        });
    }

    @Override
    public void setTextCharacter(TextView tvCharacter) {
        tvCharacter.setText(textCharacter);
    }

    @Override
    public void setTextQuote(TextView tvQuote) {
        tvQuote.setText(textQuote);
    }


    @Override
    public void start() {

    }
}
