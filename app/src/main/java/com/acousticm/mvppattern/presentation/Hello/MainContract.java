package com.acousticm.mvppattern.presentation.Hello;

import android.widget.EditText;

import com.acousticm.mvppattern.bases.BasePresenter;
import com.acousticm.mvppattern.bases.BaseView;

/**
 * Created by Zephy on 3/1/2018.
 */

public class MainContract {
    interface View extends BaseView<Presenter>{
        void clearText(String text);
        void showMessageSuccess(String text);
    }

    interface Presenter extends BasePresenter{
        boolean validateTextFields(EditText[] fields);
        boolean clearTextFields(EditText[] fields);
    }
}
