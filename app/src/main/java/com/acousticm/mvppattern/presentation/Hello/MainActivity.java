package com.acousticm.mvppattern.presentation.Hello;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.acousticm.mvppattern.R;
import com.acousticm.mvppattern.bases.BaseActivity;
import com.acousticm.mvppattern.databinding.ActivityMainBinding;
import com.acousticm.mvppattern.presentation.ShowData.ShowDataActivity;

import static android.support.v4.util.Preconditions.checkNotNull;

public class  MainActivity extends BaseActivity implements MainContract.View, View.OnClickListener {
    ActivityMainBinding binding;
    MainContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initInstances();
    }

    public void initInstances() {
        setSupportActionBar(binding.toolbar);
        binding.btnSend.setOnClickListener(this);
        binding.btnNext.setOnClickListener(this);
        binding.btnClearText.setOnClickListener(this);
    }

    @Override
    public void showMsg(String msg) {
        super.showMsg(msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                mPresenter.validateTextFields(new EditText[]{binding.etnText});
                break;
            case R.id.btnClearText:
                mPresenter.clearTextFields(new EditText[]{binding.etnText});
                break;
            case R.id.btnNext:
                Intent intent = new Intent(this, ShowDataActivity.class);
                startActivity(intent);
                break;
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setPresenter(@NonNull MainContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void clearText(String text) {
        binding.etnText.setText(text);
    }

    @Override
    public void showMessageSuccess(String text) {
        showMsg(text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter = new MainPresenter(this, this);
    }
}
