package com.acousticm.mvppattern.Service;

import android.content.Context;

import com.acousticm.mvppattern.Contextor;
import com.acousticm.mvppattern.model.GameofthroneModel;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by Zephy on 3/2/2018.
 */

public class HttpManager {

    private static HttpManager instance;

    public static HttpManager getInstance()
    {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }
    private GotService service;
    private static String BASE_HTTP = "https://got-quotes.herokuapp.com/";

    private HttpManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_HTTP)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(GotService.class);
    }

    public GotService getService(){
        return service;
    }

    public interface GotService {
        @GET("quotes")
        Call<GameofthroneModel> getData();
    }
}
