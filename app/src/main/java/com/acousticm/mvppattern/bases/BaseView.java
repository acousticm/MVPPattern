package com.acousticm.mvppattern.bases;

/**
 * Created by Zephy on 3/1/2018.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
