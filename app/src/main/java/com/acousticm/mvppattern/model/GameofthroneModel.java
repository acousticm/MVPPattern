package com.acousticm.mvppattern.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Zephy on 3/2/2018.
 */

public class GameofthroneModel {
    @SerializedName("quote")
    @Expose
    private String quote;
    @SerializedName("character")
    @Expose
    private String character;

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

}
